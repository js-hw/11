let passInput1 = document.getElementById("input1");
let passInput2 = document.getElementById("input2");
let form = document.querySelector("form");

passInput1.focus();

//Створення попереджувальних повідомлень:

let label1 = document.querySelector('#input1').closest("label");
textAlarm1 = document.createElement('span');
textAlarm1.textContent = "Please, enter values!";
textAlarm1.style = "color: red; font-size: 13px; margin: -20px 0 15px 0;"
textAlarm1.hidden = true;
label1.after(textAlarm1);

let label2 = document.querySelector('#input2').closest("label");
textAlarm2 = document.createElement('span');
textAlarm2.textContent = "You must enter the same values!";
textAlarm2.style = "color: red; font-size: 13px; margin: -20px 0 15px 0; display: none";
label2.after(textAlarm2);

//Клавіша ESC:

document.addEventListener('keydown', function (event) {
  if (event.key === 'Escape') {
    passInput1.value = "";
    passInput2.value = "";
    
    textAlarm1.hidden = true;
    textAlarm2.style.display = 'none'

    passInput1.classList.remove('border-red');
    passInput2.classList.remove('border-red');
    passInput1.focus();
}
})

//Порівняння значень в input-ах:

function pressBtn(event) {
  event.preventDefault();

  let passValue1 = passInput1.value;
  let passValue2 = passInput2.value;

  if (passValue1 === "" && passValue2 === "") {
    textAlarm2.style.display = 'none'
    textAlarm1.hidden = false;

    passInput1.focus();
    passInput2.classList.remove('border-red');    
    passInput1.classList.add('border-red');

  } else if (passValue1 == passValue2) {
    passInput1.classList.remove('border-red');
    passInput2.classList.remove('border-red');

    alert(`Welcome!`);

    form.submit()
  } else {
    textAlarm1.hidden = true;

    if (textAlarm2.style.display === 'none') {
      textAlarm2.style.display = 'block'
    }

    passInput1.classList.remove('border-red');    
    passInput2.classList.add('border-red');    
    passInput2.focus();
  }
}

//Поява та приховування іконки "ока":

function passToggle(input, img) {
  let passVis = document.getElementById(input);

  let type = passVis.getAttribute("type");
  if (type === "password") {
    type = "text";
  } else {
    type = "password";
  }

  passVis.setAttribute("type", type);

  img.classList.toggle("fa-eye");
  img.classList.toggle("fa-eye-slash");
}